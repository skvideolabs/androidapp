package com.care.hemacare;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.care.hemacare.Fragments.BloodRequests;
import com.care.hemacare.Fragments.ComplaintsFragment;
import com.care.hemacare.Fragments.ContactUsFragment;
import com.care.hemacare.Fragments.DonorListFragment;
import com.care.hemacare.Fragments.EventsFragment;
import com.care.hemacare.Fragments.FeedbacksFragment;
import com.care.hemacare.Fragments.MessagesFragment;
import com.care.hemacare.Fragments.PatientListFragment;
import com.care.hemacare.Fragments.ProfileFragment;
import com.care.hemacare.Fragments.PromotionsFragment;
import com.care.hemacare.Fragments.RequestBloodFragment;
import com.care.hemacare.Fragments.SuggestionsFragment;
import com.care.hemacare.Utils.PHelper;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.security.Permission;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    PHelper pHelper;
    ProfileFragment profileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pHelper = new PHelper(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setTitle("Request For Blood");
        if(!pHelper.getLogin()){
            openLoginActivity();
        }
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.getMenu().clear();
        if(pHelper.getRole().equalsIgnoreCase("patient")){
            navigationView.inflateMenu(R.menu.activity_main_drawer_patient);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new RequestBloodFragment()).commit();
        }else{
            navigationView.inflateMenu(R.menu.activity_main_drawer_donor);
            setTitle("Blood Requests");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new BloodRequests()).commit();
        }
        View headerView = navigationView.getHeaderView(0);
        final ImageView profile = headerView.findViewById(R.id.imageView);
        final TextView name = headerView.findViewById(R.id.userName);
        TextView role = headerView.findViewById(R.id.userRole);
        if(pHelper.getPicture()!=null){
            Picasso.get().load("http://hemacare.tk/public/"+pHelper.getPicture()).into(profile, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    profile.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_user, null));
                }
            });
        }
        name.setText(pHelper.getName());
        role.setText(pHelper.getRole());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if(R.id.nav_request_blood == id){
            setTitle("Request Blood");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new RequestBloodFragment()).commit();
        }else if(R.id.nav_profile == id){
            setTitle("Profile");
            profileFragment = new ProfileFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, profileFragment).commit();
        }else if(R.id.nav_messages == id){
            setTitle("Messages");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MessagesFragment()).commit();
        }else if(R.id.nav_events == id){
            setTitle("Events");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new EventsFragment()).commit();
        }else if(R.id.nav_promotions == id){
            setTitle("Promotions");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new PromotionsFragment()).commit();
        }else if(R.id.nav_feedback == id){
            setTitle("Feedbacks");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new FeedbacksFragment()).commit();
        }else if(R.id.nav_complaints == id){
            setTitle("Complaints");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ComplaintsFragment()).commit();
        }else if(R.id.nav_suggestions == id){
            setTitle("Suggestions");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new SuggestionsFragment()).commit();
        }else if(R.id.nav_contact_us == id){
            setTitle("Contact US");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ContactUsFragment()).commit();
        }else if(R.id.nav_logout == id){
            logout();
        }else if(R.id.nav_donor == id){
            setTitle("Donors List");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new DonorListFragment()).commit();
        }else if(R.id.nav_patient == id){
            setTitle("Patients List");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new PatientListFragment()).commit();
        }else if(R.id.nav_blood_requests == id){
            setTitle("Blood Requests");
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new BloodRequests()).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void openLoginActivity(){
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    void logout(){
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        pHelper.logout();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                    }
                })
                .setNegativeButton("cancel", null)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1000 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            profileFragment.onPermissionGranted();
        }
    }
}
