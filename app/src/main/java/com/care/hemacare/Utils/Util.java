package com.care.hemacare.Utils;

public class Util {
    public static final String BASE_URL = "http://hemacare.tk/api/";

    public static final String LOGIN = BASE_URL + "login";
    //POST: {login, password}
    public static final String REGISTER = BASE_URL + "register";
    //POST: {name, cnic, phone, age, gender, blood, location, register_as, lat, lng, email, password}
    public static final String SEND_RESET_EMAIL = BASE_URL + "sendResetEmail";
    //POST: {email}
    public static final String ACTIVE_DONORS = BASE_URL + "Patient/activeDonors";
    //GET
    public static final String ACTIVE_PATIENTS = BASE_URL + "Donor/activePatients";
    //GET
    public static final String EVENTS = BASE_URL + "AllEvents";

    public static final String REQUEST_BLOOD = BASE_URL + "Patient/BloodRequest";

    public static final String ACTIVE_BLOOD_REQUESTS = BASE_URL + "Donor/activeBloodRequests/";

    public static final String LOCATION_BROADCAST_URL = BASE_URL + "updateLocation/";

    public static final String LOCATION_GET_URL = BASE_URL+"getCurrentLocation/";

    public static final String ACCEPT_REQUEST = BASE_URL+"Donor/acceptBloodRequest/";

    public static final String SEND_FEEDBACK = BASE_URL+"SendFeedback/";

    public static final String MY_FEEDBACK = BASE_URL+"MyFeedback/";

    public static final String CANCEL_PATIENT_REQUEST = BASE_URL+"CancelRequest";

    public static final String CANCEL_DONOR_REQUEST = BASE_URL+"Donor/cancelBloodRequest/";

    public static final String MARK_BLOOD_REQUEST_COMPLETED = BASE_URL+"JobDone/";

    public static final String MARK_DONOR_AVAILABLE = BASE_URL+"Donor/MarkAvailable/";

    public static final String UPDATE_PROFILE = BASE_URL+"update_info/";

    public static final String UPDATE_PASSWORD = BASE_URL+"update_password/";

    public static final String UPDATE_PICTURE = BASE_URL+"update_avatar/";

    public static final String ALL_CHATS = BASE_URL+"Messenger/";

    public static final String NEW_CHAT = BASE_URL+"chatThread/"/*{sender}/{receiver}*/;

    public static final String GET_MESSAGES = BASE_URL+"getMessages/"/*{chat_id}*/;

    public static final String SEND_MESSAGE = BASE_URL+"sendMessage/"/*{user}/{id}*/;

    public static final String VOUCHER = BASE_URL+"Vouchers";
}
