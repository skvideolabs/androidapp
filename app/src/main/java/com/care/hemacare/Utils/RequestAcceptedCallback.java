package com.care.hemacare.Utils;

public interface RequestAcceptedCallback{
    void onRequestAccepted(String request);
}