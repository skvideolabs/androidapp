package com.care.hemacare.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PHelper {
    private SharedPreferences sharedPreferences;

    public PHelper(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setLogin(boolean flag){
        sharedPreferences.edit().putBoolean("login", flag).apply();
    }

    public void setUserId(int id){
        sharedPreferences.edit().putInt("id", id).apply();
    }

    public void setName(String name){
        sharedPreferences.edit().putString("name", name).apply();
    }

    public void setCNIC(String cnic){
        sharedPreferences.edit().putString("cnic", cnic).apply();
    }

    public void setEmail(String email){
        sharedPreferences.edit().putString("email", email).apply();
    }

    public void setPhone(String phone){
        sharedPreferences.edit().putString("phone", phone).apply();
    }

    public void setAge(int age){
        sharedPreferences.edit().putInt("age", age).apply();
    }

    public void setGender(String gender){
        sharedPreferences.edit().putString("gender", gender).apply();
    }

    public void setBlood(String blood){
        sharedPreferences.edit().putString("blood", blood).apply();
    }

    public void setLocation(String location){
        sharedPreferences.edit().putString("location", location).apply();
    }

    public void setLatLng(String latLng){
        sharedPreferences.edit().putString("latLng", latLng).apply();
    }

    public void setPicture(String picture){
        sharedPreferences.edit().putString("picture", picture).apply();
    }

    public void setStatus(String status){
        sharedPreferences.edit().putString("status", status).apply();
    }

    public void setRole(String role){
        sharedPreferences.edit().putString("role", role).apply();
    }


    public int getUserId(){
        return sharedPreferences.getInt("id", 0);
    }

    public boolean getLogin(){
        return sharedPreferences.getBoolean("login", false);
    }

    public String getName(){
        return sharedPreferences.getString("name", "");
    }

    public String getCNIC(){
        return sharedPreferences.getString("cnic", "");
    }

    public String getEmail(){
        return sharedPreferences.getString("email","");
    }

    public String getPhone(){
        return sharedPreferences.getString("phone", "");
    }

    public int getAge(){
        return sharedPreferences.getInt("age", 0);
    }

    public String getGender(){
        return sharedPreferences.getString("gender", "");
    }

    public String getBlood(){
        return sharedPreferences.getString("blood", "");
    }

    public String getLocation(){
        return sharedPreferences.getString("location", "");
    }

    public String getLatLng(){
        return sharedPreferences.getString("latLng", "");
    }

    public String getPicture(){
        return sharedPreferences.getString("picture", "");
    }

    public String getStatus(){
        return sharedPreferences.getString("status", "");
    }

    public String getRole(){
        return sharedPreferences.getString("role", "");
    }

    public void logout() {
        sharedPreferences.edit().clear().apply();
    }

}
