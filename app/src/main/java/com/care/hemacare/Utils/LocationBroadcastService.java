package com.care.hemacare.Utils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.care.hemacare.Fragments.BloodRequests;
import com.care.hemacare.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

public class LocationBroadcastService extends Service {
    private PHelper pHelper;
    public static boolean IS_RUNNING = false;
    private FusedLocationProviderClient fusedLocationClient;
    boolean isReady = true;
    private static final long UPDATE_INTERVAL = 2000, FASTEST_INTERVAL = 2000;
    LocationRequest locationRequest;
    LocationCallback locationCallback;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pHelper = new PHelper(this);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            startForeground(888, getNotification());
        }
        return START_NOT_STICKY;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                broadcastLocation(location);
            }
        });

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Location mLocation = locationResult.getLocations().get(0);
                broadcastLocation(mLocation);
            }


        };

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        IS_RUNNING = true;
    }


    private void broadcastLocation(final Location location){
        if(isReady){
            isReady = false;
            AndroidNetworking.post(Util.LOCATION_BROADCAST_URL+pHelper.getUserId())
                    .addBodyParameter("lat", location.getLatitude()+"")
                    .addBodyParameter("lng", location.getLongitude()+"")
                    .setTag("Bcast")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            isReady = true;
                        }

                        @Override
                        public void onError(ANError anError) {
                            isReady = true;
                            Toast.makeText(LocationBroadcastService.this, "Location error", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    @Override
    public void onDestroy() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
        IS_RUNNING = false;
    }


    private Notification getNotification() {
        final NotificationCompat.Builder builder = getNotificationBuilder(this,
                getPackageName()+".CHANNEL_ID_FOREGROUND", // Channel id
                NotificationManagerCompat.IMPORTANCE_LOW); //Low importance prevent visual appearance for this notification channel on top
        builder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle("Location Broadcast")
                .setContentText("Broadcasting live location");
        return builder.build();
    }

    public static NotificationCompat.Builder getNotificationBuilder(Context context, String channelId, int importance) {
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            prepareChannel(context, channelId, importance);
            builder = new NotificationCompat.Builder(context, channelId);
        } else {
            builder = new NotificationCompat.Builder(context);
        }
        return builder;
    }

    @TargetApi(26)
    private static void prepareChannel(Context context, String id, int importance) {
        final String appName = context.getString(R.string.app_name);
        String description = "No Desc";
        final NotificationManager nm = (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);

        if(nm != null) {
            NotificationChannel nChannel = nm.getNotificationChannel(id);

            if (nChannel == null) {
                nChannel = new NotificationChannel(id, appName, importance);
                nChannel.setDescription(description);
                nm.createNotificationChannel(nChannel);
            }
        }
    }
}
