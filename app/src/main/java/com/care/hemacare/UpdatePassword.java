package com.care.hemacare;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdatePassword extends AppCompatActivity {

    EditText mCurrentPassword;
    EditText mNewPassword;
    EditText mRepeatPassword;

    Button mUpdate;

    PHelper pHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        pHelper = new PHelper(this);
        setTitle("Update Password");
        mCurrentPassword = findViewById(R.id.currentPassword);
        mNewPassword = findViewById(R.id.newPassword);
        mRepeatPassword = findViewById(R.id.repeatPassword);
        mUpdate = findViewById(R.id.updatePassword);
        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    final ProgressDialog dialog = ProgressDialog.show(UpdatePassword.this, null, "Please wait...");
                    AndroidNetworking.post(Util.UPDATE_PASSWORD+pHelper.getUserId())
                            .addBodyParameter("current_password", mCurrentPassword.getText().toString())
                            .addBodyParameter("password", mNewPassword.getText().toString())
                            .setTag("ProfileUpdate")
                            .setPriority(Priority.MEDIUM)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response1) {
                                    dialog.dismiss();
                                    try{
                                        if(response1.has("success")){
                                            Toast.makeText(UpdatePassword.this, response1.getString("success"), Toast.LENGTH_SHORT).show();
                                            finish();
                                        }else{
                                            Toast.makeText(UpdatePassword.this, response1.getString("error"), Toast.LENGTH_SHORT).show();
                                        }
                                    }catch (JSONException ex){
                                        ex.printStackTrace();
                                    }
                                }
                                @Override
                                public void onError(ANError error) {
                                    dialog.dismiss();
                                    error.printStackTrace();
                                }
                            });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    private boolean validate(){
        boolean flag = true;
        if(mCurrentPassword.getText().length()==0){
            mCurrentPassword.setError("Password cannot be empty");
            flag = false;
        }
        if(mNewPassword.getText().length()==0){
            mNewPassword.setError("New password cannot be empty");
            flag = false;
        }

        if(!mRepeatPassword.getText().toString().equals(mNewPassword.getText().toString())){
            mRepeatPassword.setError("Password don't match");
            flag = false;
        }
        return flag;
    }
}
