package com.care.hemacare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {
    Button mRegister;
    PHelper pHelper;
    EditText mName, mCNIIC, mAge, mPhone, mLocation, mPassword, mRepeatPassword, mEmail;
    Spinner mBloodGroup, mGender, mUserType;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mRegister = findViewById(R.id.register);
        mEmail = findViewById(R.id.email);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    register();
                }
            }
        });
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Register");
        pHelper = new PHelper(this);
        mName = findViewById(R.id.name);
        mCNIIC = findViewById(R.id.cnic);
        mAge = findViewById(R.id.age);
        mPhone = findViewById(R.id.phoneNumber);
        mLocation = findViewById(R.id.location);
        mPassword = findViewById(R.id.password);
        mRepeatPassword = findViewById(R.id.repeatPassword);
        mBloodGroup = findViewById(R.id.bloodGroup);
        mGender = findViewById(R.id.gender);
        mUserType = findViewById(R.id.userRole);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Creating account...");
        mProgressDialog.setCancelable(false);
    }

    void openMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    void register(){
        mProgressDialog.show();
        AndroidNetworking.post(Util.REGISTER)
                .addBodyParameter("name",mName.getText().toString())
                .addBodyParameter("cnic", mCNIIC.getText().toString())
                .addBodyParameter("phone", mPhone.getText().toString())
                .addBodyParameter("age", mAge.getText().toString())
                .addBodyParameter("gender", mGender.getSelectedItem().toString())
                .addBodyParameter("blood", mBloodGroup.getSelectedItem().toString())
                .addBodyParameter("location", mLocation.getText().toString())
                .addBodyParameter("register_as", mUserType.getSelectedItem().toString())
                .addBodyParameter("lat", 0.0+"")
                .addBodyParameter("lng", 0.0+"")
                .addBodyParameter("email", mEmail.getText().toString())
                .addBodyParameter("password", mPassword.getText().toString())
                .setTag("Register")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response1) {
                        try {
                            if(mProgressDialog.isShowing()){
                                mProgressDialog.dismiss();
                            }
                            JSONObject response = null;
                            if(response1.has("user")){
                                response = response1.getJSONObject("user");
                            }
                            if(response.has("errors")){
                                Toast.makeText(RegisterActivity.this, "Error occurred!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(RegisterActivity.this, "Account created successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        if(mProgressDialog.isShowing()){
                            mProgressDialog.dismiss();
                        }                        Toast.makeText(RegisterActivity.this, "Network error!!", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    boolean validate(){
        boolean flag = true;
        if(mName.getText().toString().isEmpty()){
            mName.setError("Name cannot be empty");
            flag = false;
        }
        if(mEmail.getText().toString().isEmpty()){
            mEmail.setError("Enter a valid email");
            flag = false;
        }
        if(mCNIIC.getText().toString().isEmpty()){
            mCNIIC.setError("Enter a valid CNIC");
            flag = false;
        }
        if(mGender.getSelectedItemPosition()==0){
            Toast.makeText(this, "Please select gender", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        if(mUserType.getSelectedItemPosition()==0){
            Toast.makeText(this, "Please select user type", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        if(mBloodGroup.getSelectedItemPosition()==0){
            Toast.makeText(this, "Please select blood group", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        if(mPassword.getText().toString().length()<6){
            mPassword.setError("Password should be atlest 6 characters");
            flag = false;
        }
        if(!mRepeatPassword.getText().toString().equals(mPassword.getText().toString())){
            mPassword.setError("Passwords don't match");
            flag = false;
        }
        if(mAge.getText().toString().isEmpty()){
            Toast.makeText(this, "Age cannot be empty", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        if(mLocation.getText().toString().isEmpty()){
            Toast.makeText(this, "Location cannot be empty", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        return flag;
    }

}
