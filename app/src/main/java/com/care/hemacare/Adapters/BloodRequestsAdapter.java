package com.care.hemacare.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Data.DBloodRequest;
import com.care.hemacare.Fragments.BloodRequests;
import com.care.hemacare.MessagesActivity;
import com.care.hemacare.R;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.RequestAcceptedCallback;
import com.care.hemacare.Utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BloodRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DBloodRequest> mList;
    RequestAcceptedCallback callback;
    ProgressDialog progressDialog;
    PHelper pHelper;

    public BloodRequestsAdapter(Context context, ArrayList<DBloodRequest> mList, RequestAcceptedCallback callback) {
        this.mList = mList;
        this.callback = callback;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        pHelper = new PHelper(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RequestsViewHolder(LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.d_blood_request_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder1, int position) {
        final RequestsViewHolder holder = (RequestsViewHolder)holder1;
        final DBloodRequest bloodRequest = mList.get(position);
        holder.mName.setText(bloodRequest.getName());
        holder.mAddress.setText(bloodRequest.getLocation());
        holder.mDate.setText(bloodRequest.getDate());
        Picasso.get().load("http://hemacare.tk/public/"+bloodRequest.getImage()).into(holder.mProfile, new Callback() {
            @Override
            public void onSuccess() {}

            @Override
            public void onError(Exception e) {
                holder.mProfile.setImageDrawable(ResourcesCompat.getDrawable(holder.itemView.getResources(), R.drawable.ic_user, null));
            }
        });
        holder.mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRequest(bloodRequest.getRequestId());
            }
        });

        holder.mMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewChatThread(holder1.itemView.getContext(), bloodRequest.getUserId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class RequestsViewHolder extends RecyclerView.ViewHolder {
        ImageView mProfile;
        TextView mName, mAddress, mDate;
        Button mAccept;
        Button mMessage;

        public RequestsViewHolder(@NonNull View itemView) {
            super(itemView);
            mProfile = itemView.findViewById(R.id.profile);
            mName = itemView.findViewById(R.id.name);
            mAddress = itemView.findViewById(R.id.location);
            mDate = itemView.findViewById(R.id.date);
            mAccept = itemView.findViewById(R.id.acceptRequest);
            mMessage = itemView.findViewById(R.id.message);
        }
    }

    void acceptRequest(int id){
        progressDialog.show();
        String url = Util.ACCEPT_REQUEST+pHelper.getUserId()+"/"+id;
        AndroidNetworking.get(url)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            callback.onRequestAccepted(response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                    }
                });
    }

    void createNewChatThread(final Context context, int id){
        progressDialog.show();
        String url = Util.NEW_CHAT+pHelper.getUserId()+"/"+id;
        AndroidNetworking.get(url)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            Intent intent = new Intent(context, MessagesActivity.class);
                            intent.putExtra("chatId", response.getInt("id")+"");
                            context.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                    }
                });
    }
}
