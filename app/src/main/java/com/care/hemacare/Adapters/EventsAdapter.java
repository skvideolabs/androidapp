package com.care.hemacare.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.care.hemacare.Data.Event;
import com.care.hemacare.R;
import com.care.hemacare.Utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EventsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Event> mEvents;

    public EventsAdapter(ArrayList<Event> mEvents) {
        this.mEvents = mEvents;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new EventsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        EventsViewHolder eventsViewHolder = (EventsViewHolder)viewHolder;
        Event event = mEvents.get(i);
        eventsViewHolder.mEventTitle.setText(event.getName());
        eventsViewHolder.mEventDate.setText(event.getDate().split(" ")[0]);
        eventsViewHolder.mEventLocation.setText(event.getLocation());
        eventsViewHolder.mEventOrganizer.setText(event.getOrganizer());
        eventsViewHolder.mEventTitle.setText(event.getName());
        Picasso.get().load("http://hemacare.tk/public/"+event.getImage()).into(eventsViewHolder.mEventImage);
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    class EventsViewHolder extends RecyclerView.ViewHolder{
        TextView mEventTitle, mEventLocation, mEventDate, mEventOrganizer;
        ImageView mEventImage;

        public EventsViewHolder(@NonNull View itemView) {
            super(itemView);
            mEventTitle = itemView.findViewById(R.id.event_title);
            mEventImage = itemView.findViewById(R.id.event_image);
            mEventLocation = itemView.findViewById(R.id.event_location);
            mEventDate = itemView.findViewById(R.id.event_date);
            mEventOrganizer = itemView.findViewById(R.id.event_organizer);

        }
    }
}
