package com.care.hemacare.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.care.hemacare.Data.UserProfile;
import com.care.hemacare.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<UserProfile> mProfiles;

    public UsersAdapter(ArrayList<UserProfile> mProfiles) {
        this.mProfiles = mProfiles;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UsersViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.users_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        UserProfile userProfile = mProfiles.get(i);
        final UsersViewHolder usersViewHolder = (UsersViewHolder)viewHolder;
        usersViewHolder.mName.setText(userProfile.getName());
        usersViewHolder.mGender.setText("Gender: "+userProfile.getGender());
        usersViewHolder.mAge.setText("Age: "+userProfile.getAge()+"");
        usersViewHolder.mBloodGroup.setText("Blood Group: "+userProfile.getBloodGroup());
        usersViewHolder.mGender.setText("Gender: "+userProfile.getGender());
        usersViewHolder.mPhone.setText(userProfile.getPhone());
        Picasso.get().load("http://hemacare.tk/public/"+userProfile.getPicture()).into(usersViewHolder.mProfile, new Callback() {
            @Override
            public void onSuccess() { }

            @Override
            public void onError(Exception e) {
                usersViewHolder.mProfile.setImageDrawable(ResourcesCompat.getDrawable(usersViewHolder.itemView.getResources(), R.drawable.ic_user, null));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProfiles.size();
    }

    class UsersViewHolder extends RecyclerView.ViewHolder{
        ImageView mProfile;
        TextView mName, mPhone, mEmail, mBloodGroup, mAge, mGender;

        public UsersViewHolder(@NonNull View itemView) {
            super(itemView);
            mProfile = itemView.findViewById(R.id.profile);
            mName = itemView.findViewById(R.id.name);
            mEmail = itemView.findViewById(R.id.email);
            mBloodGroup = itemView.findViewById(R.id.blood_group);
            mAge = itemView.findViewById(R.id.age);
            mGender = itemView.findViewById(R.id.gender);
            mPhone = itemView.findViewById(R.id.phone);
        }
    }
}
