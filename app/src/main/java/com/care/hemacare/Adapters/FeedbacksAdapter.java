package com.care.hemacare.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.care.hemacare.Data.Feedback;
import com.care.hemacare.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class FeedbacksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Feedback> mList;

    public FeedbacksAdapter(ArrayList<Feedback> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FeedbacksViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        Feedback feedback = mList.get(position);
        FeedbacksViewHolder holder = (FeedbacksViewHolder)holder1;
        holder.mMessage.setText(feedback.getMessage());
        if(!feedback.getResponse().equalsIgnoreCase("null")){
            holder.mResponse.setText(feedback.getResponse());
        }
        try {
            holder.mDate.setText(new SimpleDateFormat("dd MMM, yyyy").format(new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(feedback.getCreatedAt())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class FeedbacksViewHolder extends RecyclerView.ViewHolder{
        TextView mMessage, mResponse, mDate;

        public FeedbacksViewHolder(@NonNull View itemView) {
            super(itemView);
            mMessage = itemView.findViewById(R.id.message);
            mResponse = itemView.findViewById(R.id.response);
            mDate = itemView.findViewById(R.id.date);
        }
    }
}
