package com.care.hemacare.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.care.hemacare.Data.AcceptedDonor;
import com.care.hemacare.Fragments.RequestBloodFragment;
import com.care.hemacare.Fragments.SentRequestsFragment;
import com.care.hemacare.LiveMonitorActivity;
import com.care.hemacare.R;
import com.care.hemacare.Utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AcceptedDonorsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<AcceptedDonor> mList;

    public AcceptedDonorsAdapter(ArrayList<AcceptedDonor> mList) {
        this.mList = mList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AcceptedDonors(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.accepted_donors_list_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {
        final AcceptedDonor acceptedDonor = mList.get(position);
        final AcceptedDonors holder = (AcceptedDonors)holder1;
        holder.mName.setText(acceptedDonor.getName());
        holder.mLocation.setText(acceptedDonor.getLocation());
        Picasso.get().load("http://hemacare.tk/public/"+acceptedDonor.getImage()).into(holder.mProfile, new Callback() {
            @Override
            public void onSuccess() { }

            @Override
            public void onError(Exception e) {
                holder.mProfile.setImageDrawable(ResourcesCompat.getDrawable(holder.itemView.getResources(), R.drawable.ic_user, null));
            }
        });
        holder.mTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), LiveMonitorActivity.class);
                intent.putExtra("lat", acceptedDonor.getLat());
                intent.putExtra("lng", acceptedDonor.getLng());
                intent.putExtra("userId", acceptedDonor.getUserId());
                holder.itemView.getContext().startActivity(intent);
            }
        });
        holder.mMarkComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markCompleted(holder.itemView.getContext(), acceptedDonor.getUserId(), SentRequestsFragment.id, holder.mMarkComplete, holder.mTrack);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class AcceptedDonors extends RecyclerView.ViewHolder{

        TextView mName, mLocation;
        ImageView mProfile;
        Button mTrack;
        Button mMarkComplete;

        public AcceptedDonors(@NonNull View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.name);
            mLocation = itemView.findViewById(R.id.location);
            mProfile = itemView.findViewById(R.id.profile);
            mTrack = itemView.findViewById(R.id.trackLocation);
            mMarkComplete = itemView.findViewById(R.id.markComplete);
        }
    }

    void markCompleted(final Context context, int userid, int id, final Button btn, final Button track){
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.v("SrcUrl", Util.MARK_BLOOD_REQUEST_COMPLETED + id+"/"+userid);
        AndroidNetworking.get(Util.MARK_BLOOD_REQUEST_COMPLETED + id+"/"+userid)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String strres) {
                        Log.v("ResponseServ", strres);
                        progressDialog.dismiss();
                        track.setEnabled(false);
                        btn.setEnabled(false);
                        btn.setText("Completed");
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Toast.makeText(context, "Error occurred ", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
