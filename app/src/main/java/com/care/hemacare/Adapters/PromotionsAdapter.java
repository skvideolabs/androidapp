package com.care.hemacare.Adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.care.hemacare.Data.Promotion;
import com.care.hemacare.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.CLIPBOARD_SERVICE;

public class PromotionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Promotion> mPromotions;

    public PromotionsAdapter(ArrayList<Promotion> mPromotions) {
        this.mPromotions = mPromotions;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.promotion_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder myViewHolder = (MyViewHolder)holder;
        final Promotion promotion = mPromotions.get(position);
        myViewHolder.mPromoTitle.setText(promotion.getName());
        myViewHolder.mDescription.setText(promotion.getDetail());
        myViewHolder.mPromoDate.setText(promotion.getOn_date().split(" ")[0]);
        myViewHolder.mPromoCode.setText("#"+promotion.getCode());
        Picasso.get().load("http://hemacare.tk/public/"+promotion.getImage()).into(myViewHolder.mPromoImage);
        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("promo", promotion.getCode());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(v.getContext(), "Promo code copied to clipboard.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPromotions.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mPromoTitle, mPromoCode, mPromoDate, mDescription;
        ImageView mPromoImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mPromoTitle = itemView.findViewById(R.id.promo_name);
            mPromoCode = itemView.findViewById(R.id.promo_code);
            mPromoDate = itemView.findViewById(R.id.promo_date);
            mDescription = itemView.findViewById(R.id.promo_description);
            mPromoImage = itemView.findViewById(R.id.promo_image);
        }
    }
}
