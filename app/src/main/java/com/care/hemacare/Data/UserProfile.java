package com.care.hemacare.Data;

public class UserProfile {
    int id;
    String name;
    String cnic;
    String email;
    String phone;
    int age;
    String gender;
    String location;
    String latlng;
    String picture;
    String bloodGroup;


    public void setUserId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCNIC(String cnic) {
        this.cnic = cnic;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setLatLng(String s) {
        this.latlng = latlng;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getUserId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCNIC() {
        return cnic;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getLocation() {
        return location;
    }

    public String getLatLng() {
        return latlng;
    }

    public String getPicture() {
        return picture;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }
}
