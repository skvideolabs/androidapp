package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.care.hemacare.R;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestBloodFragment extends Fragment implements OnMapReadyCallback {
    GoogleMap googleMap;
    public RequestBloodFragment() {
        // Required empty public constructor
    }

    Place mSelectedPlace;
    Button mRequestBlood;
    EditText mRequiredBottles;
    EditText mVoucherCode;
    PHelper pHelper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_blood, container, false);
//        mapView = view.findViewById(R.id.mapView);
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapView);
        pHelper = new PHelper(getContext());
        mapFragment.getMapAsync(this);
        mRequestBlood = view.findViewById(R.id.btnRequestBlood);
        mVoucherCode = view.findViewById(R.id.voucherCode);
        mRequiredBottles = view.findViewById(R.id.requiredBottles);
        Places.initialize(getContext().getApplicationContext(), "AIzaSyBHAe3WUuRT6xr94hd4wt8EiCwhcIqUCgk");

        PlacesClient placesClient = Places.createClient(getContext());

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                mSelectedPlace = place;
                googleMap.clear();
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
                MarkerOptions markerOptions  = new MarkerOptions();
                markerOptions.position(place.getLatLng());
                markerOptions.title(place.getAddress());
                googleMap.addMarker(markerOptions);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
            }
        });
        mRequestBlood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    requestBlood();
                }
            }
        });
        checkBloodRequest();

        return view;
    }

    private void requestBlood() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        AndroidNetworking.post(Util.REQUEST_BLOOD+"/"+pHelper.getUserId())
                .addBodyParameter("location", ""+mSelectedPlace.getName())
                .addBodyParameter("lat", ""+mSelectedPlace.getLatLng().latitude+"")
                .addBodyParameter("lng", ""+mSelectedPlace.getLatLng().longitude+"")
                .addBodyParameter("location_url", "https://www.google.com/maps?q="+mSelectedPlace.getLatLng()
                                                                                    .latitude+","+mSelectedPlace.getLatLng().longitude)
                .addBodyParameter("bottle_required", mRequiredBottles.getText().toString())
                .addBodyParameter("code", mVoucherCode.getText().toString())
                .setTag("RequestBlood")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject bloodRequest = response.getJSONObject("bloodRequest");
                            Log.v("BloodRequest", response.toString());
                            if(bloodRequest!=null){
                                int id = bloodRequest.getInt("id");

                                Bundle bundle =  new Bundle();
                                bundle.putString("res", response.toString());
                                getActivity().setTitle("Blood Requests");
                                Fragment fragment = new SentRequestsFragment();
                                fragment.setArguments(bundle);
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();

                        Toast.makeText(getContext(), "Error occurred!!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean validate() {
        if(mRequiredBottles.getText().toString().isEmpty()||Integer.parseInt(mRequiredBottles.getText().toString())>5){
            Toast.makeText(getContext(), "You cannot request more than 5 bottles at same time!", Toast.LENGTH_LONG).show();
            return false;
        }else if(mSelectedPlace == null){
            Toast.makeText(getContext(), "Please select a place!!", Toast.LENGTH_LONG).show();
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(31.497754, 74.360106), 15));
    }


    void checkBloodRequest(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.REQUEST_BLOOD+"/"+pHelper.getUserId())
                .setTag("RequestBlood")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject bloodRequest = response.getJSONObject("bloodRequest");
                            if(bloodRequest!=null){
                                int id = bloodRequest.getInt("id");
                                Bundle bundle =  new Bundle();
                                bundle.putString("res", response.toString());
                                getActivity().setTitle("Blood Requests");
                                Fragment fragment = new SentRequestsFragment();
                                fragment.setArguments(bundle);
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();
                    }
                });

    }
}
