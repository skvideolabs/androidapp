package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Data.Dialog;
import com.care.hemacare.Data.DialogsFixtures;
import com.care.hemacare.Data.Message;
import com.care.hemacare.Data.User;
import com.care.hemacare.MessagesActivity;
import com.care.hemacare.R;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment implements DialogsListAdapter.OnDialogClickListener {

    private DialogsList dialogsList;
    private DialogsListAdapter dialogsAdapter;
    protected ImageLoader imageLoader;

    PHelper pHelper;


    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        pHelper = new PHelper(getContext());
        dialogsList = view.findViewById(R.id.dialogsList);
        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(final ImageView imageView, String url, Object payload) {
                Picasso.get().load("http://hemacare.tk/public/"+url).into(imageView, new Callback() {
                    @Override
                    public void onSuccess() { }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_user, null));
                    }
                });
            }
        };
        loadMessages();
        return view;
    }

    private void initAdapter(ArrayList<Dialog> mList) {
        dialogsAdapter = new DialogsListAdapter<>(imageLoader);
        dialogsAdapter.setItems(mList);
        dialogsAdapter.setOnDialogClickListener(this);
        dialogsList.setAdapter(dialogsAdapter);
    }

    @Override
    public void onDialogClick(IDialog dialog) {
        Intent intent = new Intent(getContext(), MessagesActivity.class);
        intent.putExtra("chatId", dialog.getId());
        startActivity(intent);
    }

    private void loadMessages(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.ALL_CHATS+pHelper.getUserId())
                .setTag("RequestBlood")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        ArrayList<Dialog> mChats = new ArrayList<>();
                        try{
                            for(int i=0;  i<response.length(); i++){
                                JSONObject object = response.getJSONObject(i);
                                JSONObject userObject = object.getJSONObject("user_2");

                                User user = new User(String.valueOf(userObject.getInt("id")), userObject.getString("name"), userObject.getString("picture"), false);
                                ArrayList<User> users = new ArrayList<>();
                                users.add(user);
                                Message message = new Message(String.valueOf(object.getJSONObject("message").getInt("id")),
                                        user,
                                        object.getJSONObject("message").getString("message"),
                                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(object.getJSONObject("message").getString("created_at")));
                                Dialog dialog = new Dialog(String.valueOf(object.getInt("id")),
                                        object.getJSONObject("user_2").getString("name"),
                                        object.getJSONObject("user_2").getString("picture"),
                                        users,
                                        message,
                                        0);
                                mChats.add(dialog);
                            }
                            initAdapter(mChats);
                        }catch (JSONException ex){
                            Log.v("JSONException", ex.getMessage());
                            ex.printStackTrace();
                        }catch (ParseException ex){
                            Log.v("ParseException", ex.getMessage());
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }
}
