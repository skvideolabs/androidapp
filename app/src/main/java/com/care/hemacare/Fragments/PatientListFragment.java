package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.care.hemacare.Adapters.UsersAdapter;
import com.care.hemacare.Data.UserProfile;
import com.care.hemacare.R;
import com.care.hemacare.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientListFragment extends Fragment {

    ArrayList<UserProfile> mPatients;
    RecyclerView mRecyclerView;
    UsersAdapter mAdapter;
    TextView mNoPatients;

    public PatientListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_list, container, false);
        mPatients = new ArrayList<>();
        mAdapter = new UsersAdapter(mPatients);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        mNoPatients = view.findViewById(R.id.no_patients);
        getPatients();
        return view;
    }

    void getPatients(){
        final ProgressDialog dialog = ProgressDialog.show(getContext(), null, "Please wait", false);
        AndroidNetworking.get(Util.ACTIVE_PATIENTS)
                .setTag("Donors")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray responses) {
                        Log.e("ServerRes", responses.toString());
                        dialog.dismiss();
                        try {
                            for (int i=0; i<responses.length(); i++){
                                JSONObject response = responses.getJSONObject(i);
                                UserProfile profile = new UserProfile();
                                profile.setUserId(response.getInt("id"));
                                profile.setName(response.getString("name"));
                                profile.setCNIC(response.getString("cnic"));
                                profile.setEmail(response.getString("email"));
                                profile.setPhone(response.getString("phone"));
                                profile.setAge(response.getInt("age"));
                                profile.setGender(response.getString("gender"));
                                profile.setLocation(response.getString("location"));
                                profile.setLatLng(response.getString("lat")+","+response.getString("lng"));
                                profile.setPicture(response.getString("picture"));
                                profile.setBloodGroup(response.getString("blood"));
                                mPatients.add(profile);
                            }

                            if(responses.length()==0){
                                mNoPatients.setVisibility(View.GONE);
                            }

                            mAdapter.notifyDataSetChanged();
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        Toast.makeText(getContext(), "Error occurred", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
