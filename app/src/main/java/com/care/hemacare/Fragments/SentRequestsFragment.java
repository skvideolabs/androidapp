package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.care.hemacare.Adapters.AcceptedDonorsAdapter;
import com.care.hemacare.Data.AcceptedDonor;
import com.care.hemacare.LoginActivity;
import com.care.hemacare.MainActivity;
import com.care.hemacare.R;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SentRequestsFragment extends Fragment {


    public SentRequestsFragment() {
        // Required empty public constructor
    }

    TextView mBottles, mLocation, mStatus;
    Button mDelete;
    RecyclerView mRecyclerView;
    ArrayList<AcceptedDonor> mList = new ArrayList<>();
    AcceptedDonorsAdapter mAdapter;
    PHelper pHelper;

    public static int id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sent_requests, container, false);
        pHelper = new PHelper(getContext());
        Bundle bundle = getArguments();
        String res = bundle.getString("res");
        mBottles = view.findViewById(R.id.bottles);
        mLocation = view.findViewById(R.id.location);
        mStatus = view.findViewById(R.id.status);
        mDelete = view.findViewById(R.id.delete);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mAdapter = new AcceptedDonorsAdapter(mList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);

        parseJSON(res);
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage("Are you sure you want to delete request?")
                        .setCancelable(false)
                        .setPositiveButton("delete", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteRequest();
                            }
                        })
                        .setNegativeButton("cancel", null)
                        .show();
            }
        });
        return view;
    }

    private void parseJSON(String res){
        try{
            JSONObject jsonObject = new JSONObject(res);
            JSONObject bloodRequest = jsonObject.getJSONObject("bloodRequest");
            JSONArray users = jsonObject.getJSONArray("users");
            mBottles.setText(bloodRequest.getInt("bottle_required")+"");
            mLocation.setText(bloodRequest.getString("location"));
            if(bloodRequest.has("status")){
                mStatus.setText(bloodRequest.getString("status"));
            }
            id = bloodRequest.getInt("id");
            for(int i=0; i<users.length(); i++){
                JSONObject object = users.getJSONObject(i);
                AcceptedDonor acceptedDonor = new AcceptedDonor();
                acceptedDonor.setUserId(object.getInt("id"));
                acceptedDonor.setName(object.getString("name"));
                acceptedDonor.setEmail(object.getString("email"));
                acceptedDonor.setImage(object.getString("picture"));
                acceptedDonor.setLocation(object.getString("location"));
                acceptedDonor.setLat(bloodRequest.getDouble("lat"));
                acceptedDonor.setLng(bloodRequest.getDouble("lng"));
                mList.add(acceptedDonor);
            }
            mAdapter.notifyDataSetChanged();
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    void deleteRequest(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.CANCEL_PATIENT_REQUEST+"/"+id)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String strres) {
                        Log.v("ResponseServ", strres);
                        progressDialog.dismiss();
                        try {
                            JSONObject response = new JSONObject(strres);
                            if (response.getString("success").equals("Your request for blood is cancelled")){
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new RequestBloodFragment()).commit();
                                Toast.makeText(getContext(), response.getString("success"), Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getContext(), "Could not delete blood request", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Error while deleting request "+id, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
