package com.care.hemacare.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.care.hemacare.Adapters.BloodRequestsAdapter;
import com.care.hemacare.Data.DBloodRequest;
import com.care.hemacare.R;
import com.care.hemacare.Utils.LocationBroadcastService;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.RequestAcceptedCallback;
import com.care.hemacare.Utils.Util;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BloodRequests extends Fragment implements RequestAcceptedCallback {


    public BloodRequests() {
        // Required empty public constructor
    }

    PHelper pHelper;
    TextView mBottles, mLocation, mStatus;

    Button mShareLocation, mGetDirections;
    boolean isSharingLocation = false;

    double mDestLat, mDestLng;
    View mRipple;
    RippleBackground mContent;
    View mAcceptedRequestLayout;
    ArrayList<DBloodRequest> mList = new ArrayList<>();
    BloodRequestsAdapter mAdapter;
    RecyclerView mRecyclerView;
    Button mDelete;
    int id;
    LinearLayout mParentView;
    RelativeLayout mChildView;
    Button markAvailable;
    TextView mNoRequests;

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blood_requests, container, false);
        pHelper = new PHelper(getContext());
        mBottles = view.findViewById(R.id.bottles);
        mLocation = view.findViewById(R.id.location);
        mStatus = view.findViewById(R.id.status);
        mNoRequests = view.findViewById(R.id.no_requests);
        mGetDirections = view.findViewById(R.id.get_directions);
        mShareLocation = view.findViewById(R.id.share_live_location);
        markAvailable = view.findViewById(R.id.markAvailable);
        mRipple = view.findViewById(R.id.ripple);
        mContent = view.findViewById(R.id.content);
        mAcceptedRequestLayout = view.findViewById(R.id.acceptedRequest);
        mAdapter = new BloodRequestsAdapter(getContext(), mList, this);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mParentView = view.findViewById(R.id.parentView);
        mChildView = view.findViewById(R.id.childView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        mShareLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LocationBroadcastService.class);
                if(!isSharingLocation){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                        if(!LocationBroadcastService.IS_RUNNING){
                            getActivity().startForegroundService(intent);
                        }
                    }else{
                        getActivity().startService(intent);
                    }
                    isSharingLocation = true;
                    mShareLocation.setText("Stop sharing");
                    mRipple.setVisibility(View.VISIBLE);
                    mContent.startRippleAnimation();
                }else{
                    isSharingLocation = false;
                    mShareLocation.setText("Share live location");
                    getActivity().stopService(intent);
                    mRipple.setVisibility(View.GONE);
                    mContent.stopRippleAnimation();
                }
            }
        });
        mGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("google.navigation:q="+mDestLat+","+mDestLng));
                startActivity(intent);
            }
        });
        mDelete = view.findViewById(R.id.delete);
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage("Are you sure you want to cancel request?")
                        .setCancelable(false)
                        .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteRequest();
                            }
                        })
                        .setNegativeButton("no", null)
                        .show();
            }
        });
        loadBloodRequests();
        markAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAvailable();
            }
        });
        return view;
    }

    private void markAvailable() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.MARK_DONOR_AVAILABLE+pHelper.getUserId())
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String strres) {
                        progressDialog.dismiss();
                        try {
                            JSONObject response = new JSONObject(strres);
                            Toast.makeText(getContext(), response.getString("status"), Toast.LENGTH_SHORT).show();
                            loadBloodRequests();
                            mParentView.setVisibility(View.VISIBLE);
                            mChildView.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Error occurred", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    void loadBloodRequests(){
        mList.clear();
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.ACTIVE_BLOOD_REQUESTS+pHelper.getUserId())
                .setTag("BloodRequests")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            Log.e("ResponseServ", response.toString());
                            Object bloodRequest = response.getString("acceptedRequst");
                            boolean isAvailable = response.getBoolean("isAvailable");
                            if(isAvailable){
                                if(bloodRequest.equals("null")){
                                    JSONArray pendingRequests = response.getJSONArray("bloodRequest");
                                    for(int i=0; i<pendingRequests.length(); i++){
                                        JSONObject object = pendingRequests.getJSONObject(i);
                                        DBloodRequest bloodRequest1 = new DBloodRequest();
                                        bloodRequest1.setImage(object.getJSONObject("user").getString("picture"));
                                        bloodRequest1.setName(object.getJSONObject("user").getString("name"));
                                        bloodRequest1.setLocation(object.getString("location"));
                                        bloodRequest1.setEmail(object.getJSONObject("user").getString("email"));
                                        try {
                                            bloodRequest1.setDate(
                                                    new SimpleDateFormat("dd MMM, yyyy").format(new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(object.getString("updated_at")))
                                            );
                                        } catch (ParseException e) {
                                            bloodRequest1.setDate("Parse error");
                                        }
                                        bloodRequest1.setLat(object.getDouble("lat"));
                                        bloodRequest1.setLng(object.getDouble("lng"));
                                        bloodRequest1.setRequestId(object.getInt("id"));
                                        mList.add(bloodRequest1);
                                    }
                                    mAdapter.notifyDataSetChanged();
                                    mAcceptedRequestLayout.setVisibility(View.GONE);
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                    if(pendingRequests.length()==0){
                                        mNoRequests.setVisibility(View.VISIBLE);
                                    }
                                }else{
                                    JSONObject acceptedRequests = response.getJSONObject("acceptedRequst");
                                    mBottles.setText(acceptedRequests.getInt("bottle_required")+"");
                                    mLocation.setText(acceptedRequests.getString("location"));
                                    mStatus.setText(acceptedRequests.getString("status"));
                                    mDestLat = acceptedRequests.getDouble("lat");
                                    mDestLng = acceptedRequests.getDouble("lng");
                                    mAcceptedRequestLayout.setVisibility(View.VISIBLE);
                                    mRecyclerView.setVisibility(View.GONE);
                                    id = acceptedRequests.getInt("id");
                                }
                            }else {
                                mParentView.setVisibility(View.GONE);
                                mChildView.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.e("ResponseServE", error.getMessage());
                        progressDialog.dismiss();//108221330746
                        error.printStackTrace();
                    }
                });

    }

    @Override
    public void onRequestAccepted(String request) {
        loadBloodRequests();
    }

    void deleteRequest(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.CANCEL_DONOR_REQUEST+pHelper.getUserId()+"/"+id)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("ResponseServ", response.toString());
                        progressDialog.dismiss();
                        loadBloodRequests();
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Error while cancelling request "+id, Toast.LENGTH_SHORT).show();
                    }
                });
    }


}
