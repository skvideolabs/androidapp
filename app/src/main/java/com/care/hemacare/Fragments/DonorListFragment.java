package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Adapters.UsersAdapter;
import com.care.hemacare.Data.UserProfile;
import com.care.hemacare.LoginActivity;
import com.care.hemacare.R;
import com.care.hemacare.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DonorListFragment extends Fragment {

    ArrayList<UserProfile> mDonors;
    RecyclerView mRecyclerView;
    UsersAdapter mAdapter;
    TextView mNoDonors;

    public DonorListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_donor_list, container, false);
        mDonors = new ArrayList<>();
        mAdapter = new UsersAdapter(mDonors);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mNoDonors = view.findViewById(R.id.no_donors);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        getDonors();
        return view;
    }

    void getDonors(){

        final ProgressDialog dialog = ProgressDialog.show(getContext(), null, "Please wait...");
        AndroidNetworking.get(Util.ACTIVE_DONORS)
                .setTag("Donors")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray responses) {
                        dialog.dismiss();
                        try {
                            for (int i=0; i<responses.length(); i++){
                                JSONObject response = responses.getJSONObject(i);
                                UserProfile profile = new UserProfile();
                                profile.setUserId(response.getInt("id"));
                                profile.setName(response.getString("name"));
                                profile.setCNIC(response.getString("cnic"));
                                profile.setEmail(response.getString("email"));
                                profile.setPhone(response.getString("phone"));
                                profile.setAge(response.getInt("age"));
                                profile.setGender(response.getString("gender"));
                                profile.setLocation(response.getString("location"));
                                profile.setLatLng(response.getString("lat")+","+response.getString("lng"));
                                profile.setPicture(response.getString("picture"));
                                profile.setBloodGroup(response.getString("blood"));
                                mDonors.add(profile);
                            }
                            if(responses.length()==0){
                                mNoDonors.setVisibility(View.VISIBLE);
                            }
                            mAdapter.notifyDataSetChanged();
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }

}
