package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.R;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends Fragment {

    Spinner mCategory;
    EditText mMessage;
    Button mSubmit;
    PHelper pHelper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        mCategory = view.findViewById(R.id.category);
        mMessage = view.findViewById(R.id.message);
        mSubmit = view.findViewById(R.id.submit);
        pHelper = new PHelper(getContext());
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    makeRequest();
                }
            }
        });

        return view;
    }

    boolean validate(){
        boolean flag = true;
        if(mCategory.getSelectedItemPosition()==0){
            flag = false;
            Toast.makeText(getContext(), "Please select a category", Toast.LENGTH_SHORT).show();
        }
        if(mMessage.getText().length()<80){
            flag = false;
            mMessage.setError("Message should consist of atleast 80 characters");
        }
        return flag;
    }

    private void makeRequest(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.post(Util.SEND_FEEDBACK+pHelper.getUserId())
                .addBodyParameter("message",mMessage.getText().toString())
                .addBodyParameter("type", mCategory.getSelectedItem().toString())
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String type = response.getString("type");
                            String success = response.getString("success");
                            if(type.equalsIgnoreCase("Complaint")){
                                getActivity().setTitle("Complaints");
                                getActivity().getSupportFragmentManager().beginTransaction().
                                        replace(R.id.container, new ComplaintsFragment()).commit();
                            }else if(type.equalsIgnoreCase("Suggestion")){
                                getActivity().setTitle("Suggestions");
                                getActivity().getSupportFragmentManager().beginTransaction().
                                        replace(R.id.container, new SuggestionsFragment()).commit();
                            }else if(type.equalsIgnoreCase("Feedback")){
                                getActivity().setTitle("Feedbacks");
                                getActivity().getSupportFragmentManager().beginTransaction().
                                        replace(R.id.container, new FeedbacksFragment()).commit();
                            }

                            Toast.makeText(getContext(), success, Toast.LENGTH_SHORT).show();
                        }catch (JSONException ex){
                            Toast.makeText(getContext(), "Parse error!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "Error occored while submitting "+mCategory.getSelectedItem().toString(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
