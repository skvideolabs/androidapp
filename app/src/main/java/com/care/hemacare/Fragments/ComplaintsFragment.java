package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.care.hemacare.Adapters.FeedbacksAdapter;
import com.care.hemacare.Data.Feedback;
import com.care.hemacare.R;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplaintsFragment extends Fragment {


    public ComplaintsFragment() {
        // Required empty public constructor
    }

    PHelper pHelper;
    RecyclerView mRecyclerView;
    FeedbacksAdapter mAdapter;
    ArrayList<Feedback> mList = new ArrayList<>();
    TextView mNoComplaints;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_complaints, container, false);
        pHelper = new PHelper(getContext());
        mAdapter = new FeedbacksAdapter(mList);
        mNoComplaints = view.findViewById(R.id.no_complaints);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        loadComplaints();
        return view;
    }


    void loadComplaints(){
        mList.clear();
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.MY_FEEDBACK+pHelper.getUserId()+"/Complaint")
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener(){
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        Log.e("ResponseServ", response.toString());

                        try {
                            for (int i=0; i<response.length(); i++){
                                JSONObject object = response.getJSONObject(i);
                                Feedback feedback = new Feedback();
                                feedback.setMessage(object.getString("message"));
                                feedback.setResponse(object.getString("response"));
                                feedback.setCreatedAt(object.getString("created_at"));
                                feedback.setUpdatedAt(object.getString("updated_at"));
                                mList.add(feedback);
                            }
                            if(response.length()==0){
                                mNoComplaints.setVisibility(View.VISIBLE);
                            }
                            mAdapter.notifyDataSetChanged();
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }
}
