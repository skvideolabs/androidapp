package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.care.hemacare.Adapters.EventsAdapter;
import com.care.hemacare.Adapters.PromotionsAdapter;
import com.care.hemacare.Data.Event;
import com.care.hemacare.Data.Promotion;
import com.care.hemacare.R;
import com.care.hemacare.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromotionsFragment extends Fragment {

    RecyclerView mRecyclerView;
    ArrayList<Promotion> mPromotions;
    PromotionsAdapter mAdapter;
    TextView mNoPromotions;


    public PromotionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promotions, container, false);
        mPromotions = new ArrayList<>();
        mAdapter = new PromotionsAdapter(mPromotions);
        mRecyclerView =  view.findViewById(R.id.recyclerView);
        mNoPromotions = view.findViewById(R.id.no_promotions);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        loadPromos();
        return view;
    }

    void loadPromos(){
        final ProgressDialog dialog = ProgressDialog.show(getContext(), null, "Please wait", false);

        AndroidNetworking.get(Util.VOUCHER)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            dialog.dismiss();
                            for (int i=0; i<response.length(); i++){
                                JSONObject jsonObject = response.getJSONObject(i);
                                Promotion promotion = new Promotion();
                                promotion.setId(jsonObject.getInt("id"));
                                promotion.setName(jsonObject.getString("name"));
                                promotion.setImage(jsonObject.getString("image"));
                                promotion.setDetail(jsonObject.getString("detail"));
                                promotion.setCode(jsonObject.getString("code"));
                                promotion.setOn_date(jsonObject.getString("on_date"));
                                mPromotions.add(promotion);
                            }

                            if(response.length()==0){
                                mNoPromotions.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }finally {
                            mAdapter.notifyDataSetChanged();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        error.printStackTrace();
                    }
                });

    }

}
