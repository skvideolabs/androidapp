package com.care.hemacare.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.utils.Utils;
import com.bumptech.glide.Glide;
import com.care.hemacare.LoginActivity;
import com.care.hemacare.R;
import com.care.hemacare.UpdateContact;
import com.care.hemacare.UpdatePassword;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }

    Button mUpdatePassword;
    Button mUpdateProfile;
    PHelper pHelper;

    TextView mTName;
    TextView mType;

    EditText mName;
    Spinner mGender;
    Spinner mBloodGroup;
    EditText mCnic;
    EditText mAge;
    EditText mEmail;

    ImageView mProfilePicture;

    boolean isCamera = false;

    private static final int IMAGE_RESULT = 101;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        pHelper = new PHelper(getContext());
        mUpdatePassword = view.findViewById(R.id.updatePassword);
        mTName = view.findViewById(R.id.t_name);
        mProfilePicture = view.findViewById(R.id.image);
        mType = view.findViewById(R.id.userType);
        mName = view.findViewById(R.id.name);
        mGender = view.findViewById(R.id.gender);
        mCnic = view.findViewById(R.id.cnic);
        mBloodGroup = view.findViewById(R.id.bloodGroup);
        mAge = view.findViewById(R.id.age);
        mEmail = view.findViewById(R.id.email);
        mUpdateProfile = view.findViewById(R.id.updateProfile);

        mUpdatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), UpdatePassword.class));
            }
        });

        mUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    updateProfile();
                }
            }
        });
        mProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    } else {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                1000);
                    }
                }else {
                    startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT);
                }
            }
        });
        showInfo();
        return view;
    }

    private boolean validate(){
        boolean flag = true;

        if(mName.getText().length()==0){
            flag = false;
            mName.setError("Name cannot be empty");
        }

        if(mBloodGroup.getSelectedItemPosition()==0){
            flag = false;
            Toast.makeText(getContext(), "Please select blood group", Toast.LENGTH_SHORT).show();
        }


        if(mGender.getSelectedItemPosition()==0){
            flag = false;
            Toast.makeText(getContext(), "Please select gender", Toast.LENGTH_SHORT).show();
        }

        if(mCnic.getText().length()==0){
            flag = false;
            mCnic.setError("Enter CNIC");
        }

        if(mAge.getText().length()==0){
            flag = false;
            mAge.setError("Age cannot be empty");
        }

        return flag;
    }

    private void updateProfile(){
        final ProgressDialog dialog = ProgressDialog.show(getContext(), null, "Please wait...");
        AndroidNetworking.post(Util.UPDATE_PROFILE+pHelper.getUserId())
                .addBodyParameter("name", mName.getText().toString())
                .addBodyParameter("blood", mBloodGroup.getSelectedItem().toString())
                .addBodyParameter("gender", mGender.getSelectedItem().toString())
                .addBodyParameter("age", mAge.getText().toString())
                .addBodyParameter("cnic", mCnic.getText().toString())
                .setTag("ProfileUpdate")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response1) {
                        dialog.dismiss();
                        try{
                            Toast.makeText(getContext(), response1.getString("success"), Toast.LENGTH_SHORT).show();
                            JSONObject user = response1.getJSONObject("user");
                            pHelper.setName(user.getString("name"));
                            pHelper.setCNIC(user.getString("cnic"));
                            pHelper.setAge(user.getInt("age"));
                            pHelper.setGender(user.getString("gender"));
                            pHelper.setBlood(user.getString("blood"));
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }

    private void showInfo(){
        String genders[] = getResources().getStringArray(R.array.gender);
        String bloodGroups[] = getResources().getStringArray(R.array.bloodGroup);
        mTName.setText(pHelper.getName());
        mName.setText(pHelper.getName());
        mEmail.setText(pHelper.getEmail());
        mCnic.setText(pHelper.getCNIC());
        mType.setText(pHelper.getRole());
        mAge.setText(pHelper.getAge()+"");
        mBloodGroup.setSelection(getIndex(bloodGroups, pHelper.getBlood()));
        mGender.setSelection(getIndex(genders, pHelper.getGender()));
        Picasso.get().load("http://hemacare.tk/public/"+pHelper.getPicture()).into(mProfilePicture, new Callback() {
            @Override
            public void onSuccess() { }

            @Override
            public void onError(Exception e) {
                mProfilePicture.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_user, null));
            }
        });
    }

    private int getIndex(String[] arr, String text){
        int index = -1;
        for (int i=0;i<arr.length;i++) {
            if (arr[i].equals(text)) {
                index = i;
                break;
            }
        }
        return index;
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            Log.v("ImagesIntents", res.activityInfo.packageName);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        Intent intent = new Intent(galleryIntent);
        intent.setComponent(new ComponentName("com.google.android.apps.photos", "com.google.android.apps.photos.picker.external.ExternalPickerActivity"));
        allIntents.add(intent);

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent1 : allIntents) {
            if (intent1.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent1;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_RESULT) {
                boolean isCamera = data == null || data.getData() == null;
                String filePath;
                if(isCamera){
                    filePath = getCaptureImageOutputUri().getPath();
                }else {
                    filePath = getPathFromURI(data.getData());
                }

                if(filePath!=null){
                    uploadImage(filePath);
                }else {
                    Toast.makeText(getContext(), "File path is null", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void uploadImage(String filePath){
        final ProgressDialog dialog = ProgressDialog.show(getContext(), null, "Please wait...");
        AndroidNetworking.upload(Util.UPDATE_PICTURE+pHelper.getUserId())
                .addMultipartFile("picture", new File(filePath))
                .setTag("ProfileUpdate")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();

                        try{
                            JSONObject response1 = new JSONObject(response);
                            Toast.makeText(getContext(), response1.getString("success"), Toast.LENGTH_SHORT).show();
                            Picasso.get().load("http://hemacare.tk/public/"+response1.getJSONObject("user").getString("picture")).into(mProfilePicture);
                            pHelper.setPicture(response1.getJSONObject("user").getString("picture"));
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }


    public void onPermissionGranted(){
        startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT);
    }
}
