package com.care.hemacare.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Adapters.EventsAdapter;
import com.care.hemacare.Data.Event;
import com.care.hemacare.R;
import com.care.hemacare.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {

    RecyclerView mRecyclerView;
    ArrayList<Event> mEvents;
    EventsAdapter mAdapter;
    TextView mNoEvents;

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        mEvents = new ArrayList<>();
        mAdapter = new EventsAdapter(mEvents);
        mRecyclerView =  view.findViewById(R.id.recyclerView);
        mNoEvents = view.findViewById(R.id.no_events);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
        loadEvents();
        return view;
    }

    void loadEvents(){
        final ProgressDialog dialog = ProgressDialog.show(getContext(), null, "Please wait", false);

        AndroidNetworking.get(Util.EVENTS)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            dialog.dismiss();
                            for (int i=0; i<response.length(); i++){
                                JSONObject jsonObject = response.getJSONObject(i);
                                Event event = new Event();
                                event.setId(jsonObject.getInt("id"));
                                event.setName(jsonObject.getString("name"));
                                event.setImage(jsonObject.getString("image"));
                                event.setLocation(jsonObject.getString("location"));
                                event.setLat(jsonObject.getDouble("lat"));
                                event.setLng(jsonObject.getDouble("lng"));
                                event.setLocationUrl(jsonObject.getString("location_url"));
                                event.setDate(jsonObject.getString("on_date"));
                                event.setPhone(jsonObject.getString("phone"));
                                event.setOrganizer(jsonObject.getString("organizer"));
                                mEvents.add(event);
                            }

                            if(response.length()==0){
                                mNoEvents.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }finally {
                            mAdapter.notifyDataSetChanged();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        dialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }
}
