package com.care.hemacare;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    Button mLogin, mRegister;
    EditText mEmail, mPassword;
    PHelper pHelper;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");
        mLogin = findViewById(R.id.login);
        mRegister = findViewById(R.id.register);
        pHelper = new PHelper(this);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterActivity();
            }
        });
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    login();
                }
            }
        });
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
    }

    void openRegisterActivity(){
        startActivity(new Intent(this, RegisterActivity.class));
    }

    void openMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void forgotPassword(View view){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Forgot Password");
        alertDialog.setMessage("Enter Email");

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Send reset password link", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(input.getText().toString().isEmpty()){
                    input.setError("Please enter a valid email");
                }else{
                    resetPassword(input.getText().toString());
                }
            }
        });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();

    }


    void login(){
        mProgressDialog.show();
        AndroidNetworking.post(Util.LOGIN)
                .addBodyParameter("email",mEmail.getText().toString())
                .addBodyParameter("password", mPassword.getText().toString())
                .setTag("Login")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response1) {
                        try {
                            if(mProgressDialog.isShowing()){
                                mProgressDialog.dismiss();
                            }
                            JSONObject response = null;
                            if(response1.has("user")){
                                response = response1.getJSONObject("user");
                            }
                            if(response1.has("email_error")){
                                Toast.makeText(LoginActivity.this, response1.getString("email_error"), Toast.LENGTH_SHORT).show();
                            }else if(response1.has("password_error")){
                                Toast.makeText(LoginActivity.this, response1.getString("password_error"), Toast.LENGTH_SHORT).show();
                            }else if(response1.has("errors")){
                                Toast.makeText(LoginActivity.this, "Error occurred!!", Toast.LENGTH_SHORT).show();
                            }else{
                                pHelper.setUserId(response.getInt("id"));
                                pHelper.setName(response.getString("name"));
                                pHelper.setCNIC(response.getString("cnic"));
                                pHelper.setEmail(response.getString("email"));
                                pHelper.setPhone(response.getString("phone"));
                                pHelper.setAge(response.getInt("age"));
                                pHelper.setGender(response.getString("gender"));
                                pHelper.setLocation(response.getString("location"));
                                pHelper.setLatLng(response.getString("lat")+","+response.getString("lng"));
                                pHelper.setPicture(response.getString("picture"));
                                pHelper.setStatus(response.getString("status"));
                                if(response.getJSONArray("roles").length()>0){
                                    pHelper.setRole(response.getJSONArray("roles").getJSONObject(0).getString("name"));
                                }else {
                                    pHelper.setRole("UA");
                                }

                                pHelper.setLogin(true);
                                openMainActivity();
                            }
                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        if(mProgressDialog.isShowing()){
                            mProgressDialog.dismiss();
                        }
                        Toast.makeText(LoginActivity.this, "Networking error!!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void resetPassword(String email){
        AndroidNetworking.post(Util.SEND_RESET_EMAIL)
                .addBodyParameter("email",email)
                .setTag("ResetPass")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                    @Override
                    public void onError(ANError error) {

                    }
                });
    }

    boolean validate(){
        boolean flag = true;
        if(mEmail.getText().toString().isEmpty()){
            mEmail.setError("Enter a valid email");
            flag= false;
        }
        if(mPassword.getText().toString().isEmpty()){
            mPassword.setError("Password must be consist of atleast 6 characters");
            flag= false;
        }
        return flag;
    }
}
