package com.care.hemacare;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.care.hemacare.Data.Dialog;
import com.care.hemacare.Data.Message;
import com.care.hemacare.Data.MessagesFixtures;
import com.care.hemacare.Data.User;
import com.care.hemacare.Utils.PHelper;
import com.care.hemacare.Utils.Util;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class MessagesActivity extends AppCompatActivity implements MessageInput.InputListener,
        MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnLoadMoreListener{

    private MessagesList messagesList;

    protected final String senderId = "0";
    protected ImageLoader imageLoader;
    protected MessagesListAdapter<Message> messagesAdapter;


    PHelper pHelper;

    int lastLoadedMessagesCount = 0;
    boolean isAlreadyLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        pHelper = new PHelper(this);
        this.messagesList = (MessagesList) findViewById(R.id.messagesList);
        MessageInput input = (MessageInput) findViewById(R.id.input);
        input.setInputListener(this);
        setTitle("Chat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initAdapter();
        loadMessages();
    }

    @Override
    public boolean onSubmit(CharSequence input) {
        Message message = new Message(MessagesFixtures.getRandomId(),
                new User("0", pHelper.getName(), pHelper.getPicture(), false), input.toString());
        messagesAdapter.addToStart(message, true);
        AndroidNetworking.post(Util.SEND_MESSAGE+pHelper.getUserId()+"/"+getIntent().getStringExtra("chatId"))
                .addBodyParameter("message", input.toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                         JSONObject r = response;
                    }
                    @Override
                    public void onError(ANError error) {
                        error.printStackTrace();
                    }
                });
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public void onSelectionChanged(int count) {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {

    }

    protected void loadMessages() {
//        new Handler().postDelayed(new Runnable() { //imitation of internet connection
//            @Override
//            public void run() {
//                ArrayList<Message> messages = MessagesFixtures.getMessages(lastLoadedDate);
//                lastLoadedDate = messages.get(messages.size() - 1).getCreatedAt();
//                messagesAdapter.addToEnd(messages, false);
//            }
//        }, 1000);

        //messagesAdapter.addToEnd();

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AndroidNetworking.get(Util.GET_MESSAGES+getIntent().getStringExtra("chatId"))
                .setTag("RequestBlood")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        ArrayList<Message> mMessages = new ArrayList<>();
                        try{
                            lastLoadedMessagesCount = response.length();
                            for(int i=0;  i<response.length(); i++){
                                JSONObject object = response.getJSONObject(i);
                                JSONObject userObject = object.getJSONObject("user");
                                boolean ownMessage = userObject.getInt("id") == pHelper.getUserId();

                                User user = new User(ownMessage?"0":String.valueOf(userObject.getInt("id")),
                                        userObject.getString("name"),
                                        userObject.getString("picture"),
                                        false);

                                Message message = new Message(String.valueOf(object.getInt("id")),
                                        user,
                                        object.getString("message"),
                                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(object.getString("created_at")));
                                mMessages.add(message);
                            }
                            Collections.reverse(mMessages);
                            messagesAdapter.addToEnd(mMessages, false);
                            startLooping();
                        }catch (JSONException ex){
                            Log.v("JSONException", ex.getMessage());
                            ex.printStackTrace();
                        }catch (ParseException ex){
                            Log.v("ParseException", ex.getMessage());
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();
                    }
                });
    }

    private void initAdapter() {
        messagesAdapter = new MessagesListAdapter<>(senderId, imageLoader);
        this.messagesList.setAdapter(messagesAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    void startLooping(){
        new MessageFetcherAsyncTask().execute();
    }

    void loadRecentMessages(){
        if(!isAlreadyLoading){
            isAlreadyLoading = true;
            AndroidNetworking.get(Util.GET_MESSAGES + getIntent().getStringExtra("chatId"))
                    .setTag("RequestBlood")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            isAlreadyLoading = false;
                            try{
                                if(lastLoadedMessagesCount<response.length()){
                                    lastLoadedMessagesCount = response.length();

                                    JSONObject object = response.getJSONObject(response.length()-1);
                                    JSONObject userObject = object.getJSONObject("user");
                                    boolean ownMessage = userObject.getInt("id") == pHelper.getUserId();

                                    if(!ownMessage){
                                        User user = new User(ownMessage?"0":String.valueOf(userObject.getInt("id")),
                                                userObject.getString("name"),
                                                userObject.getString("picture"),
                                                false);

                                        Message message = new Message(String.valueOf(object.getInt("id")),
                                                user,
                                                object.getString("message"),
                                                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(object.getString("created_at")));
                                        messagesAdapter.addToStart(message, true);
                                    }
                                }
                            }catch (JSONException ex){
                                Log.v("JSONException", ex.getMessage());
                                ex.printStackTrace();
                            }catch (ParseException ex){
                                Log.v("ParseException", ex.getMessage());
                                ex.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            isAlreadyLoading = false;
                            error.printStackTrace();
                        }
                    });
        }
    }


    class MessageFetcherAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
                while (true){
                    loadRecentMessages();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

        }
    }
}
