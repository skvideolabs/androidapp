package com.care.hemacare;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.care.hemacare.Utils.Util;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LiveMonitorActivity extends AppCompatActivity implements OnMapReadyCallback, RoutingListener {

    GoogleMap googleMap;
    boolean isMapReady = false;
    String url = null;
    LatLng mDestination;
    int userId;
    PolylineOptions polylineOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_monitor);
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        mDestination = new LatLng(getIntent().getDoubleExtra("lat", 0),getIntent().getDoubleExtra("lng", 0));
        handler = new Handler();
        setTitle("Live Location");
        userId = getIntent().getIntExtra("userId", 0);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        isMapReady = true;
        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mDestination, 15));
        fetchLocation();
    }

    boolean isFetching = false;

    void fetchLocation(){
        if(!isFetching&&isMapReady){
            AndroidNetworking.get(Util.LOCATION_GET_URL+userId)
                    .setTag("FetchLocation")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            isFetching = false;
                            try {
                                double lat = response.getDouble("lat");
                                double lng = response.getDouble("lng");
                                updateMap(new LatLng(lat, lng));
                                if(url==null){
                                    downloadPolyline(mDestination, new LatLng(lat, lng));
                                    url = "Hi";
                                }
                            }catch (JSONException ex){
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            isFetching = false;
                            anError.printStackTrace();
                        }
                    });
            isFetching = true;

        }
    }

    LatLng mStart;
    void updateMap(LatLng latLng){
        googleMap.clear();

        if(mStart==null){
            mStart = latLng;
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title("Destination")
                .position(mDestination);
        googleMap.addMarker(markerOptions);

        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.title("Current Location");
        markerOptions1.position(latLng);
        markerOptions1.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        googleMap.addMarker(markerOptions1);
        if(polylineOptions!=null){
            googleMap.addPolyline(polylineOptions);
        }
    }

    boolean flag = true;
    Handler handler;

    @Override
    public void onRoutingFailure(RouteException e) {
        RouteException exception = e;
        e.printStackTrace();
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
        int jj=0;
        polylineOptions = arrayList.get(0).getPolyOptions().color(Color.RED).width(14f);
        googleMap.addPolyline(polylineOptions);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(mDestination);
        builder.include(mStart);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(builder.build().getCenter(), 12));
        new FetcherAsyncTask().execute();
    }

    @Override
    public void onRoutingCancelled() {

    }

    void downloadPolyline(LatLng start, LatLng end){
        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(start, end)
                .key("AIzaSyBHAe3WUuRT6xr94hd4wt8EiCwhcIqUCgk")
                .build();
        routing.execute();
    }

    class FetcherAsyncTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            while (flag){
                fetchLocation();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }


}
